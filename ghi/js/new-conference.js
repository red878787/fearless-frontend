window.addEventListener('DOMContentLoaded', async () => {

    // Fetch available locations and populate select tag
    const locationsUrl = 'http://localhost:8000/api/locations/';
    const response = await fetch(locationsUrl);

    if (response.ok) {
      const data = await response.json();
      const selectTag = document.getElementById('location');

      for (let location of data.locations) {
        const optionElement = document.createElement('option');
        optionElement.value = location.id;
        optionElement.innerHTML = location.name;
        selectTag.appendChild(optionElement);
      }
    }

    // Handle form submission
    const formTag = document.getElementById('create-conference-form');

    formTag.addEventListener('submit', async event => {
      event.preventDefault();

      const formData = new FormData(formTag);
      const jsonData = JSON.stringify(Object.fromEntries(formData));

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: jsonData,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
      }
    });
  });