window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIcon = document.getElementById('loading-conference-spinner');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // Add the 'd-none' class to the loading icon
      loadingIcon.classList.add('d-none');

      // Remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none');
    }
    const form = document.getElementById('create-attendee-form');
    const successMessage = document.getElementById('success-message');
    const errorMessage = document.getElementById('error-message');  // You will need an error message HTML element

    form.addEventListener('submit', async (event) => {
      // Prevent the form from submitting normally
      event.preventDefault();

      // Create a FormData object from the form
      const formData = new FormData(form);

      // Convert the FormData entries into a simple object
      const formDataObject = Object.fromEntries(formData.entries());

      // Define options for the fetch request
      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formDataObject),
      };

      // Make the fetch request
      const response = await fetch('http://localhost:8001/api/attendees/', options);

      // If the fetch was successful, hide the form and show the success message
      if (response.ok) {
        form.classList.add('d-none');
        successMessage.classList.remove('d-none');
      } else {
        // If the fetch was not successful, display the error message
        errorMessage.classList.remove('d-none');
      }
    });
  });