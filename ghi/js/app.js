function showAlert(message) {
  const alertPlaceholder = document.getElementById('alert-placeholder');
  alertPlaceholder.innerHTML = `
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      ${message}
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  `;
}

function createCard(title, description, pictureUrl, startDate, endDate, locationName) {
  return `
    <div class="card shadow mb-3">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        <small class="text-muted">${startDate} - ${endDate}</small>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {
  const columns = [
      document.querySelector('.col-1'),
      document.querySelector('.col-2'),
      document.querySelector('.col-3')
  ];
  let columnIndex = 0;

  const url = 'http://localhost:8000/api/conferences/';

  try {
      const response = await fetch(url);

      if (!response.ok) {
          console.error('Something went wrong, got an error in the response!');
      } else {
          const data = await response.json();
          for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const title = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const locationName = details.conference.location.name;

                  if (details.conference.starts && details.conference.ends) {
                    const start = new Date(details.conference.starts);
                    const end = new Date(details.conference.ends);
                    const startDate = start.toLocaleDateString();
                    const endDate = end.toLocaleDateString();

                    const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);

                    columns[columnIndex].innerHTML += html;
                } else {
                    console.error('Starts or Ends property is missing');
                }

                  columnIndex = (columnIndex + 1) % 3;
              }
          }
      }
  } catch (e) {
      showAlert('An error occurred while fetching the data. Please try again later.');
      console.log('Error:', e);
  }
});
